/*
  Bibliotecas utilizadas
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <TaskScheduler.h>
/*
  FIN Bibliotecas utilizadas
*/




/*
  A continuación se crea un objeto de la clase RF24 que será el encargado de controlar el módulo nRF24l01+.
  Los pines digitales 7 y 8 del arduino serán los encargados de controlar los pines CE y CSN (del módulo) respectivamente.
  Estos pines se pueden cambiar por cualquiera otros que no se requieran para la comunicación por medio del protocolo SPI,
  lo cual puede variar de acuerdo a la targeta arduino que se esté utilizando
 */
RF24 sensor(7, 8);

/*
 Esta constante representa la dirección que utilizarán los módulos para comunicarse y se puede utilizar cualquier
 secuencia de 5 caracteres, siempre y cuando tanto la del transmisor como la del receptor coincidan.
*/
const byte sensor_address[6] = "SENSO";


int lector_salida = A0; //pin utilizado para medir señal de salida del sensor
double salida_y = 0.0;
double Ts = 0.01;


void callbackLazo();

Task Lazo((Ts*100), TASK_FOREVER, &callbackLazo);

Scheduler RealTime;

void callbackLazo(){
    LeerSalida();
}

void LeerSalida(){
    Serial.println("leyendo salida");
    salida_y = (analogRead(lector_salida) * 5.0) / 1023.0;
    Serial.println("salida = " + String(salida_y));
    enviarSalida();
}

void enviarSalida(){
    Serial.println("Enviando datos");
    sensor.stopListening();
    sensor.write(&(salida_y), sizeof(salida_y));
}

void setup() {
    Serial.begin(9600);
    if(sensor.begin()){
        Serial.println("radio init successfully");
    }
    else{
        Serial.println("radio init successfully");
    }
    sensor.openWritingPipe(sensor_address);
    sensor.stopListening();
    sensor.setChannel(1);
    sensor.setPALevel(RF24_PA_MIN);


    RealTime.init();
    RealTime.addTask(Lazo);
    Lazo.enable();
}

void loop() {
    RealTime.execute();
}
