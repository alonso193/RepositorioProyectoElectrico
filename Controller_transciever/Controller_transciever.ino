/*
  Bibliotecas utilizadas
*/
#include <SPI.h>            //permite comunicación entre el nrf24l01 y el arduino
#include <nRF24L01.h>       //configuración del módulo nrf24l01
#include <RF24.h>           //configuración del módulo nrf24l01
#include <TaskScheduler.h>  //uso de tareas
/*
  FIN Bibliotecas utilizadas
*/



/*
  A continuación se crea un objeto de la clase RF24 que será el encargado de controlar el módulo nRF24l01+.
  Los pines digitales 7 y 8 del arduino serán los encargados de controlar los pines CE y CSN (del módulo) respectivamente.
  Estos pines se pueden cambiar por cualquiera otros que no se requieran para la comunicación por medio del protocolo SPI,
  lo cual puede variar de acuerdo a la targeta arduino que se esté utilizando
 */
RF24 controller_transciever(7, 8);


/*
 Esta constante representa la dirección que utilizarán los módulos para comunicarse y se puede utilizar cualquier
 secuencia de 5 caracteres, siempre y cuando tanto la del transmisor como la del receptor coincidan.
*/
const byte sensor_address[6] = "SENSO";
const byte actuator_address[6] = "ACTUA";


/*
 * Sección de pines y variables varias usadas en el programa
*/
int mode_select = 0;          //if == 0 manual, else automático
int LED_pin = 4;              //utilizado para indicar el modo de operación. if(manual_select) HIGH else LOW
int set_point = A0;           //pin analógico utilizado para establecer set point
int U_manual = A1;            //pin analógico utilizado para establecer señal de control de modo manual
int botton_mode_select = 5;   //pin digital usado para leer boton de selección de modo de operación
int botton_reject = 6;        //pin para el botón usado para detener la operación
int antirebote = 0;           //usada para evitar rebotes del botón de selección de modo
int antirebote_anterior = 0;  //guarda el estado anterior del antirebote
int killProcess;


/*
 *Tomando en cuenta que la FT de la planta es:
 *
    P(s) = (1,15e^(-0,075s))/(0,6424s + 1)
 *
 * A continuación las variables utilizadas para el controlador PID:
*/
double error = 0.0;
double referencia = 0.0;
double U = 0.0;
double salida_y = 0.0;
double P = 0.0;
double I = 0.0;
double I_pasado = 0.0;
double IAE = 0.0;
double Ts = 0.01;
/*
 * Ganancias del controlador
*/
//pidtune con datos del proyecto de marcela
//IAE = 1.23
double Kp = 1.202231273014860;
double Ti = Kp/3.624676446647797;
//double Kp = 1.304657871083437;
//double Ti = Kp/3.341350006151092;

//pidtune con datos de la medición directa
//IAE = 3.9
//double Kp = 1.271604444388720;//Estos dieron muy malos resultados
//double Ti = Kp/0.913850405122122;
//double Kp = 1.351414241956782;
//double Ti = Kp/0.817993297902296;

//pidtune con datos del 123c
//IAE = 4.96
//double Kp = 0.873010437965694;
//double Ti = Kp/0.710723823616929;
// double Kp = 1.265936540005970;
// double Ti = Kp/0.591824327016221;
// double Td = Kp*0.076415584758026;


/*
 * creación de la tarea que repetirá cada tiempo de muestreo
*/
void callbackLazo();
void callbackKill();

Task Lazo((Ts*1000), TASK_FOREVER, &callbackLazo);
Task Kill(0,TASK_FOREVER, &callbackKill);

Scheduler RealTime;

void callbackLazo(){
    ModoOperacion();
}

void callbackKill(){
    Killer();
}

void Killer(){
    digitalWrite(LED_pin, LOW);
    Serial.println(String(IAE));
    U = 0.00;
    controller_transciever.stopListening();
    controller_transciever.write(&U, sizeof(U));
}

void ModoOperacion(){
    killProcess = digitalRead(botton_reject);
    if(killProcess == HIGH){
        Lazo.disable();
        RealTime.deleteTask(Lazo);
        Kill.enable();
    }
    antirebote = digitalRead(botton_mode_select);
    if((antirebote == HIGH) && (antirebote_anterior == LOW)){
        mode_select = 1 - mode_select;
    }

    antirebote_anterior = antirebote;

    if(mode_select == 0){
        ManualMode();
    }
    else{//mode_select == 1
        AutomaticMode();
    }
}

void ManualMode(){
    //Serial.println("modo manual");
    digitalWrite(LED_pin, HIGH);
    U = 0.0;
    for(int i = 0; i < 50; i++){
        U = U + analogRead(U_manual);
    }

    U = U/50.00;
    U = (U*5.00)/1023.00;
    LeerY();
    Serial.println(String(U)+  "," + String(salida_y)+  "," + String(U) + ",0,0");
    ComunicarU();
}

void AutomaticMode(){
    //Serial.println("modo automático");
    digitalWrite(LED_pin, LOW);
    LeerR();
    LeerY();
    Serial.println(String(referencia) +  "," + String(salida_y) +  "," + String(U) +  ",0,0");
    CalcSalidaPID();
    ComunicarU();
}

void CalcSalidaPID(){
    error = referencia - salida_y;
    IAE += abs(error)*Ts;
    I = I_pasado + (((Kp*Ts)/Ti)*error);
    P = Kp*error;
    U = P + I;
    if(U < 0){
        U = 0.0;
    }
    if(U > 5){
        U = 5.0;
    }
    I_pasado = I;
}

void ComunicarU(){
    controller_transciever.stopListening();
    if (!controller_transciever.write(&U, sizeof(U))){
        reportarErrorControladorActuador();
    }

}

void reportarErrorControladorActuador(){
    Serial.println(String(referencia) +  "," + String(salida_y) +  "," + String(U) +  ",1,0");
}

void reportarErrorSensorControlador(){
    Serial.println(String(referencia) +  "," + String(salida_y) +  "," + String(U) +  ",0,1");
}

void LeerY(){
    controller_transciever.startListening();
    if (controller_transciever.available()){
        controller_transciever.read(&salida_y, sizeof(salida_y));
    }
    else{
        reportarErrorSensorControlador();
    }
}

void LeerR(){
    referencia = (analogRead(set_point)*5.00)/1023.00;
}




void setup() {
    Serial.begin(9600);

    pinMode(botton_mode_select, INPUT);
    pinMode(botton_reject, INPUT);
    pinMode(LED_pin, OUTPUT);

    controller_transciever.begin();
    controller_transciever.openWritingPipe(actuator_address);
    controller_transciever.openReadingPipe(1, sensor_address);
    controller_transciever.setChannel(1);
    controller_transciever.setPALevel(RF24_PA_MIN);

    RealTime.init();
    RealTime.addTask(Lazo);
    RealTime.addTask(Kill);
    Lazo.enable();
}

void loop() {
    RealTime.execute();
}
