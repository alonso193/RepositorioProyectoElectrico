#include "Arduino.h"
//******************************************************************************
const int bottonPin = 2; //pin usado para leer botón de inicio de prueba
const int pinSalida = 3; //pin usado para aplicar el escalón al actuador
const int salidaSensor = A0; //pin analógico para leer salida del sensor
const int pinEntrada = A1; //pin analógico para leer señal aplicada
int inicio = 0; //usada como bandera de inicio de la prueba
int controlRebote = 0; //usada como bandera antirebote del botón
double temporal; //usada como base para calcular el tiempo de ejecución
double tiempo; //tiempo de ejecución de la prueba
String datos;
double salida;
double entrada;
//******************************************************************************
void setup() {
    pinMode(pinSalida, OUTPUT);
    pinMode(bottonPin, INPUT);
    Serial.begin(9600);
}
//******************************************************************************
void loop() {
    if ((digitalRead(bottonPin) == HIGH) && (controlRebote == 0))
    {
        temporal = micros();
        inicio = 1;
        controlRebote = 1;
    }
    if (inicio ==  1)
    {
        tiempo = (micros() - temporal);
        analogWrite(pinSalida, ((4*255)/5));
        salida = (analogRead(salidaSensor)*5.000/1023.000);
        entrada = (analogRead(pinEntrada)*5.000/1023.000);
        datos = String(tiempo) + "," + String(salida) + "," +  String(entrada);
        Serial.println(datos);
    }
    if(tiempo/1000000 >= 25){
        analogWrite(pinSalida,0);
        Serial.println("0");
        inicio = 0;
        controlRebote = 0;
    }
}
//******************************************************************************
