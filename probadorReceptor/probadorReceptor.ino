#include <SPI.h>            //permite comunicación entre el nrf24l01 y el arduino
#include <nRF24L01.h>       //configuración del módulo nrf24l01
#include <RF24.h>

RF24 radio(7, 8);
const byte address[6] = "0000A";
int dato;

void setup(){
    pinMode(13,OUTPUT);
    Serial.begin(9600);
    if(!radio.begin()){
        Serial.println("Begin failed");
    }
    radio.openReadingPipe(0,address);
    radio.setChannel(1);
    radio.setPALevel(RF24_PA_LOW);
    radio.startListening();
}

void loop(){
    if (radio.available()){
        radio.read(&dato, sizeof(dato));
    }
    digitalWrite(13,dato);
}
