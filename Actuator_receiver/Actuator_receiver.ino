/*
  Bibliotecas utilizadas
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <TaskScheduler.h>
/*
  FIN Bibliotecas utilizadas
*/




/*
  A continuación se crea un objeto de la clase RF24 que será el encargado de controlar el módulo nRF24l01+.
  Los pines digitales 7 y 8 del arduino serán los encargados de controlar los pines CE y CSN (del módulo) respectivamente.
  Estos pines se pueden cambiar por cualquiera otros que no se requieran para la comunicación por medio del protocolo SPI,
  lo cual puede variar de acuerdo a la targeta arduino que se esté utilizando
 */
RF24 actuator(7, 8);

/*
 Esta constante representa la dirección que utilizarán los módulos para comunicarse y se puede utilizar cualquier
 secuencia de 5 caracteres, siempre y cuando tanto la del transmisor como la del receptor coincidan.
*/
const byte actuator_address[6] = "ACTUA";

double salida_controlador = 0.0;
double salida_pwm = 0;
int pin_salida = 3; //pin pwm usado para generar la salida del controlador
double Ts = 0.01;


void callbackLazo();

Task Lazo((Ts*100), TASK_FOREVER, &callbackLazo);

Scheduler RealTime;

void callbackLazo(){
    leerDato();
}


void leerDato(){
    actuator.startListening();
    if(actuator.available()){
        actuator.read(&salida_controlador, sizeof(salida_controlador));
    }
    salida_pwm = (salida_controlador*255.00)/5.00;
    escribirSalida();
}

void escribirSalida(){
    analogWrite(pin_salida, salida_pwm);
}



void setup() {
    Serial.begin(9600);

    pinMode(4,OUTPUT);
    actuator.begin();
    actuator.openReadingPipe(1, actuator_address);
    actuator.setChannel(1);
    actuator.setPALevel(RF24_PA_MIN);
    actuator.startListening();

    RealTime.init();
    RealTime.addTask(Lazo);
    Lazo.enable();
}

void loop() {
    RealTime.execute();
}
