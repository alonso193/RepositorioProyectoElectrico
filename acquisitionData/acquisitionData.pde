import processing.serial.*;
Serial puertoSerial;
Table table;
int estado = 0;
int bandera = 0;
int contador = 0;
double tiempo1;
double tiempo2;
double tiempoMuerto;
TableRow newRow;
void setup()
{
  puertoSerial = new Serial(this, "/dev/ttyACM0", 9600);
  puertoSerial.bufferUntil('\n');
  size(1000,600);
  background(255,255,255);
  for(int i = 0; i <= 6; i++){
        stroke(0);
        line(0,100*i,1000,100*i);
        fill(50);
        text((6-i),10,((100*i) - 2));
    }
  table = new Table();
  //add a column header "Data" for the collected data
  table.addColumn("Tiempo");
  table.addColumn("Salida");
  table.addColumn("Entrada");
}

void draw(){
  stroke(0,255,0);//verde para la referencia
  line(0,1000,600,0);
}

void serialEvent(Serial puertoSerial){
  String datos = puertoSerial.readString();
  float[] splitDatos = float(split(datos, ','));
  if(splitDatos.length == 3){
    newRow = table.addRow();
    newRow.setString("Tiempo",Float.toString(splitDatos[0]));
    newRow.setString("Salida",Float.toString(splitDatos[1]));
    if(splitDatos[2] != 0 && estado == 0){
      estado = 1;
      tiempo1 = splitDatos[0];
    }
    if(splitDatos[1] != 0 && bandera == 0){
      tiempo2 = splitDatos[0];
      tiempoMuerto = (tiempo2 - tiempo1)/1000000;
      println(tiempoMuerto);
      bandera = 1;
    }
    if(estado == 1){
      newRow.setString("Entrada","4");
    }
    else{
      newRow.setString("Entrada","0");
    }
  }
  else if(splitDatos.length != 3){
    saveTable(table, "data/table.csv");
    exit();
  }
}  
  
//void keyPressed()
//{
  //save as a table in csv format(data/table - data folder name table)
//  saveTable(table, "data/table.csv");
//  exit();
//}
