/*
  Bibliotecas utilizadas
*/
#include <TaskScheduler.h>  //uso de tareas
/*
  FIN Bibliotecas utilizadas
*/


/*
 Esta constante representa la dirección que utilizarán los módulos para comunicarse y se puede utilizar cualquier
 secuencia de 5 caracteres, siempre y cuando tanto la del transmisor como la del receptor coincidan.

/*
 * Sección de pines y variables varias usadas en el programa
*/
int mode_select = 0;          //if == 0 manual, else automático
int pin_salida = 3; //pin pwm usado para generar la salida del controlador
int LED_pin = 4;              //utilizado para indicar el modo de operación. if(manual_select) HIGH else LOW
int set_point = A0;           //pin analógico utilizado para establecer set point
int U_manual = A1;            //pin analógico utilizado para establecer señal de control de modo manual
int lector_salida = A3;
int botton_mode_select = 5;   //pin digital usado para leer boton de selección de modo de operación
int botton_reject = 6;        //pin para el botón usado para detener la operación
int antirebote = 0;           //usada para evitar rebotes del botón de selección de modo
int antirebote_anterior = 0;  //guarda el estado anterior del antirebote
int killProcess;





/*
 *Tomando en cuenta que la FT de la planta es:
 *
    P(s) = (1,15e^(-0,075s))/(0,6424s + 1)
 *
 * A continuación las variables utilizadas para el controlador PID:
*/
double error = 0.0;
double referencia = 0.0;
double U = 0.0;
double salida_y = 0.0;
double P = 0.0;
double I = 0.0;
double I_pasado = 0.0;
double IAE = 0.0;
double Ts = 0.01;
/*
 * Ganancias del controlador
*/
//pidtune con datos del proyecto de marcela
//IAE = 1.21
double Kp = 1.202231273014860;
double Ti = Kp/3.624676446647797;


/*
 * creación de la tarea que repetirá cada tiempo de muestreo
*/
void callbackLazo();
void callbackKill();

Task Lazo((Ts*1000), TASK_FOREVER, &callbackLazo);
Task Kill(0,TASK_FOREVER, &callbackKill);

Scheduler RealTime;

void callbackLazo(){
    ModoOperacion();
}

void callbackKill(){
    Killer();
}

void Killer(){
    digitalWrite(LED_pin, LOW);
    Serial.println(String(IAE));
    U = 0.00;
}

void ModoOperacion(){
    killProcess = digitalRead(botton_reject);
    if(killProcess == HIGH){
        Lazo.disable();
        RealTime.deleteTask(Lazo);
        Kill.enable();
    }
    antirebote = digitalRead(botton_mode_select);
    if((antirebote == HIGH) && (antirebote_anterior == LOW)){
        mode_select = 1 - mode_select;
    }

    antirebote_anterior = antirebote;

    if(mode_select == 0){
        ManualMode();
    }
    else{//mode_select == 1
        AutomaticMode();
    }
}

void ManualMode(){
    //Serial.println("modo manual");
    digitalWrite(LED_pin, HIGH);
    U = 0.0;
    for(int i = 0; i < 50; i++){
        U = U + analogRead(U_manual);
    }

    U = U/50.00;
    U = (U*5.00)/1023.00;
    LeerY();
    Serial.println(String(U)+  "," + String(salida_y)+  "," + String(U) + ",0,0");
    analogWrite(pin_salida, (U*255.00)/5.00);
}

void AutomaticMode(){
    //Serial.println("modo automático");
    digitalWrite(LED_pin, LOW);
    LeerR();
    LeerY();
    Serial.println(String(referencia) +  "," + String(salida_y) +  "," + String(U) +  ",0,0");
    CalcSalidaPID();
    analogWrite(pin_salida, (U*255.00)/5.00);
}

void CalcSalidaPID(){
    error = referencia - salida_y;
    IAE += abs(error)*Ts;
    I = I_pasado + (((Kp*Ts)/Ti)*error);
    P = Kp*error;
    U = P + I;
    if(U < 0){
        U = 0.0;
    }
    if(U > 5){
        U = 5.0;
    }
    I_pasado = I;
}



void LeerY(){
    salida_y = (analogRead(lector_salida) * 5.0) / 1023.0;
}

void LeerR(){
    referencia = (analogRead(set_point)*5.00)/1023.00;
}




void setup() {
    Serial.begin(9600);

    pinMode(botton_mode_select, INPUT);
    pinMode(botton_reject, INPUT);
    pinMode(LED_pin, OUTPUT);


    RealTime.init();
    RealTime.addTask(Lazo);
    RealTime.addTask(Kill);
    Lazo.enable();
}

void loop() {
    RealTime.execute();
}
