import processing.serial.*;

Serial puertoSerial;

//variables propias de la grafica
int xPos = 1;
int horizontalSize = 1000;
int verticalSize = 600;
int a = verticalSize/7;

//variables propias de la lectura de datos
float salida = 0.0;
float referencia = 0.0;
float salidaControlador = 0.0;
float banderaErrorControladorActuador = 0.0;
float banderaErrorSensorControlador = 0.0;
int cantidadParametros = 5;

void setup(){
  puertoSerial = new Serial(this, "/dev/ttyUSB0",9600);
  puertoSerial.bufferUntil('\n');
  
  size(1000,600);
  background(255,255,255);
  for(int i = 0; i <= 6; i++){
        stroke(0);
        line(0,a*i,horizontalSize,a*i);
        fill(50);
        text((6-i),10,((a*i) - 2));
    }
}
void draw(){
  stroke(0,255,0);//verde para la referencia
  line(xPos,verticalSize-referencia,xPos,verticalSize - 2 - referencia);
  stroke(0,0,255);//azul para la salida
  line(xPos,verticalSize-salida,xPos,verticalSize - 2 - salida);
  stroke(255,0,255);//magenta salida del controlador
  line(xPos,verticalSize-salidaControlador,xPos,verticalSize - 2 - salidaControlador);
  if(banderaErrorControladorActuador == 1.0){
    stroke(0,255,255);//cian para errores controladorActuador
    line(xPos,verticalSize,xPos,verticalSize-a);
  }
  if(banderaErrorSensorControlador == 1.0){
    stroke(255,0,0);//rojo para errores sensor controlador
    line(xPos,verticalSize,xPos,verticalSize-a);
  }
  if(xPos >= horizontalSize){
    xPos = 0;
    background(255,255,255);
    for(int i = 0; i <= 6; i++){
      stroke(0);
      line(0,a*i,horizontalSize,a*i);
      fill(50);
      text((6-i),10,((a*i) - 2));
    }
  }
  else{
    xPos++;
  }
}

void serialEvent(Serial puertoSerial){
  String datos = puertoSerial.readString();
  float[] splitDatos = float(split(datos, ','));
  if(splitDatos.length == cantidadParametros){
    referencia =  map(splitDatos[0], 0.0,5.0,0+a, verticalSize - a);
    salida = map(splitDatos[1], 0.0,5.0,0+a, verticalSize - a);
    salidaControlador = map(splitDatos[2], 0.0,5.0,0+a, verticalSize - a); 
    banderaErrorControladorActuador = splitDatos[3];
    banderaErrorSensorControlador = splitDatos[4];
  } 
  else if(splitDatos.length == 1){
    println("IAE = " + splitDatos[0]);
  }
}
