#include <SPI.h>            //permite comunicación entre el nrf24l01 y el arduino
#include <nRF24L01.h>       //configuración del módulo nrf24l01
#include <RF24.h>

RF24 radio(7, 8);

const byte address[6] = "0000A";

int dato = 1;

void setup(){
    Serial.begin(9600);
    if(!radio.begin()){
        Serial.println("Begin failed");
    }
    radio.openWritingPipe(address);
    radio.setChannel(1);
    radio.setPALevel(RF24_PA_LOW);
    radio.stopListening();
}

void loop(){
    if(radio.write(&dato,sizeof(dato))){Serial.println("crash");}
    dato = 0;
    delay(1000);
    radio.write(&dato,sizeof(dato));
    dato = 1;
    delay(1000);
}
